function demoNeurAllocs() 

NUM_NEURONS = 40;

SAVE_DIR = 'allocFigs/';
FONT_SIZE = 14;

SAVE = 0;

% initialize list of depths to test (written like this so individual values
% are easy to comment out)
DEPTHS = [];
DEPTHS = [DEPTHS 1];
DEPTHS = [DEPTHS 2];
DEPTHS = [DEPTHS 3];
DEPTHS = [DEPTHS 4];
DEPTHS = [DEPTHS 5];

F1 = [.1 .2 .3 .4];
F2 = [.1 .11 .12 .13];
F3 = 1/50 * [ones(1, 10) 2 4 6 6 10 12];    % matches Blake's diagram
F4 = .02 * [1 2 4 6 10 12];

% create bar graphs for the category frequencies that are similar to my
% simulations, and scatter plots for the frequencies that are similar to
% Blake's for ease of comparison with existing figures
for d=DEPTHS
    figure(), bar(NUM_NEURONS*calcNeuronAllocations(F1, d));
    dName = num2str(d);
    labelFigure(['Neuron Allocations - 10/20/30/40% Categories, Depth ',dName], ...
        'Category', 'Number of neurons allocated (out of 40)')
    saveFig(['freqs10-20-30-40_depth',dName])
    
    figure(), bar(NUM_NEURONS*calcNeuronAllocations(F2, d));
    labelFigure(['Neuron Allocations - 10/11/12/13% Categories, Depth ',dName], ...
        'Category', 'Number of neurons allocated (out of 40)')
    saveFig(['freqs10-11-12-13_depth',dName])
    
    figure(), scatter(F3*50, 50*calcNeuronAllocations(F3, d), 'k' );
    labelFigure(['Neuron Allocations - First Character Relative Frequencies, Depth ',dName], ...
        'First character instances', 'Number of neurons allocated (out of 50)')
    saveFig(['firstCharFreqs_depth',dName])
    
    figure(), scatter(F4*50, 50*calcNeuronAllocations(F4, d), 'k' );
    labelFigure(['Neuron Allocations - First Character Relative Frequencies, No Redundant Counts, Depth ',dName], ...
        'First character instances', 'Number of neurons allocated (out of 50)')
    saveFig(['firstCharFreqs_uniqueCounts_depth',dName])
end

    function saveFig(name)
        % make figure more legible
        setFigureFontSize(FONT_SIZE)
        
        % have y axis always start at 0
        yl = ylim(gca);
        yl(1) = 0;
        ylim(gca, yl);
        
        % save and close figure
        if (SAVE)
            fName = [SAVE_DIR,name,'.png'];
            saveas(gcf, fName);
            close
        end
    end
end