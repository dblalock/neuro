function soloMain()

NUM_RUNS = 3;       % number of networks to avg together
LOAD_DATA = 0;      % don't recreate networks?
LOAD_METRICS = 0;   % don't reanalyze networks?

NUM_INPUT_PATTERNS = 100;
NUM_TEST_PATTERNS = 1000;

% threshold values to sweep
% THRESHS = [.5 1 1.5 2];
% THRESHS = [1 1.5 2];
% THRESHS = [1.5 2];
% THRESHS = [2];
THRESHS = [];
% THRESHS = [THRESHS .5];
THRESHS = [THRESHS 1];
THRESHS = [THRESHS 1.5];
% THRESHS = [THRESHS 2];

% generate input sets using our custom function
inputSets = genSoloInputSets(NUM_INPUT_PATTERNS);
numInputSets = numel(inputSets);

% create a spy diagram for each input set
dirName = 'soloFigs/inputs/';
mkdir('soloFigs');
mkdir(dirName);
% for i=1:numInputSets
%     inputSet = inputSets{i};
%     spyInput(inputSet)
%     saveNetsPlot([dirName,inputSet.name])
% end

% create a histogram of all-pairs euclidean distances for each input set
% for i=1:numInputSets
%     inputSet = inputSets{i};
%     patterns = inputSet.inputPatterns;
%     cStartIdxs = inputSet.catStartIdxs;
%     name = ['eucDist_',inputSet.name];
%     euclideanDistsHist(patterns, cStartIdxs, name, dirName);
% end
% return

% generate test sets using our custom function
testSets = genSoloInputSets(NUM_TEST_PATTERNS);

% generate parameters for network construction
p = NetworkParams();
p.neuronCount = 20;
p.log = 1;
p.verbose = 1;

% initialize container for network metrics across sweep
allAvgMetrics = cell(numInputSets, length(THRESHS) );

% sweep threshold
tStart = tic();
for i=1:numel(THRESHS)
    thresh=THRESHS(i);
    p.threshold = thresh;

    % determine file name to save analyses as
    fileName = generateFileName('', p);
    metricsFileName = [fileName,'_metrics.mat'];
    
    % run the simulations and get the averaged results;
    % or, alternatively, just load old analyses
    if (LOAD_METRICS)
        file = load(metricsFileName);
        avgMetrics = file.avgMetrics;
        runMetrics = file.runMetrics;
    else
        [avgMetrics runMetrics] = simulate(inputSets, testSets, p, NUM_RUNS, LOAD_DATA);
        save(metricsFileName, 'avgMetrics', 'runMetrics');
    end

    % display the results
%     for j=1:numel(avgMetrics)
%         metrics = avgMetrics{j};
%         name = inputSets{j}.name;
%         fprintf('Input Set: %s\n', name);
%         dirName = ['figs/thresh',num2str(thresh*1000)];
%         visualizeNetwork(metrics, name, dirName);
%         close all
%     end
    
    % store the average metrics; we use deal() to copy
    % values over in a vectorized way
    [allAvgMetrics{:, i}] = deal(avgMetrics{:});

end
toc(tStart)

return

noiseBits = [0 1 2 4 8 12]; % property of input sets

% display classification mutual information across noise + threshold
imgStructs(allAvgMetrics, 'classificationInfo', THRESHS, noiseBits)
labelFigure('Mutual information between input and output classifications', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/classificationInfo')

% display classification mutual information across noise + threshold
imgStructs(allAvgMetrics, 'classificationBitsPerJ', THRESHS, noiseBits)
labelFigure('Bits per Joule for input and output classifications', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/classificationBitsPerJ')

% display mean activity level across noise + threshold
imgStructs(allAvgMetrics, 'meanAct', THRESHS, noiseBits)
labelFigure('Mean neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanAct')

% display min activity level across noise + threshold
imgStructs(allAvgMetrics, 'minAct', THRESHS, noiseBits)
labelFigure('Minimum neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/minAct')

% display max activity level across noise + threshold
imgStructs(allAvgMetrics, 'maxAct', THRESHS, noiseBits)
labelFigure('Maximum neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/maxAct')

% display  across noise + threshold
imgStructs(allAvgMetrics, 'nonzeroNeurons', THRESHS, noiseBits)
labelFigure('Number of nonzero neurons for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/nonzeroNeurons')

% display statistical dependence across noise + threshold
imgStructs(allAvgMetrics, 'statDep', THRESHS, noiseBits)
labelFigure('Statistical Dependence for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/statDep')

% display mean cost across noise + threshold
imgStructs(allAvgMetrics, 'meanCost', THRESHS, noiseBits)
labelFigure('Mean energy cost for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanCost')

% display mean cost per neuron across noise + threshold
imgStructs(allAvgMetrics, 'meanCostPerNeuron', THRESHS, noiseBits)
labelFigure('Mean energy cost per neuron for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanCost')

% display mutual information across noise + threshold
imgStructs(allAvgMetrics, 'mutualInfo', THRESHS, noiseBits)
labelFigure('Mutual Information for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/mutualInfo')

% display bits per joule across noise + threshold
imgStructs(allAvgMetrics, 'bitsPerJ', THRESHS, noiseBits)
labelFigure('Bits Per Joule for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/BPJ')

end