function [inputSets inputSetNames catStartIdxs inputObjs] = generateInputSets()
% [inputSets inputSetNames catStartIdxs inputObjs] = generateInputSets()
%
% Generating input for neural nets final project
%
% Everything is in caps cuz these were originally constants at the top
% of the main file

DEBUG_INPUTS = 0;
CATEGORIES_COUNT = 4;

if (DEBUG_INPUTS)
    FEATURES_COUNT = 20;
    PATTERNS_A_COUNT = 1;
    PATTERNS_B_COUNT = 2;
    PATTERNS_C_COUNT = 3;
    PATTERNS_D_COUNT = 4;
else
    FEATURES_COUNT = 64;
    PATTERNS_A_COUNT = 10;
    PATTERNS_B_COUNT = 20;
    PATTERNS_C_COUNT = 30;
    PATTERNS_D_COUNT = 40;
end

% PATTERNS_COUNT = PATTERNS_A_COUNT + PATTERNS_B_COUNT + ...
%     PATTERNS_C_COUNT + PATTERNS_D_COUNT;

% constants used to assemble actual input matrices
ZEROS_BLOCK = zeros(FEATURES_COUNT/CATEGORIES_COUNT,1);
ONES_BLOCK  = ones(FEATURES_COUNT/CATEGORIES_COUNT,1);

% prototypes for each category
PROT_A = [ONES_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK];
PROT_B = [ZEROS_BLOCK; ONES_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK];
PROT_C = [ZEROS_BLOCK; ZEROS_BLOCK; ONES_BLOCK; ZEROS_BLOCK];
PROT_D = [ZEROS_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK; ONES_BLOCK];

% matrices for each category--the prototypes replicated the 
% appropriate number of times
PATTERNS_A = repmat(PROT_A, 1, PATTERNS_A_COUNT);
PATTERNS_B = repmat(PROT_B, 1, PATTERNS_B_COUNT);
PATTERNS_C = repmat(PROT_C, 1, PATTERNS_C_COUNT);
PATTERNS_D = repmat(PROT_D, 1, PATTERNS_D_COUNT);

% the actual inputs sets
input_cntrl = [PATTERNS_A PATTERNS_B PATTERNS_C PATTERNS_D];
input_overlap1 = addOnedBitErrorsToCols(input_cntrl, 1);
input_overlap2 = addOnedBitErrorsToCols(input_cntrl, 2);
input_overlap4 = addOnedBitErrorsToCols(input_cntrl, 4);
input_overlap8 = addOnedBitErrorsToCols(input_cntrl, 8);
% input_overlap16= addOnedBitErrorsToCols(input_cntrl, 16);
input_zeroed1  = addZeroedBitErrorsToCols(input_cntrl, 1);
input_zeroed2  = addZeroedBitErrorsToCols(input_cntrl, 2);
input_zeroed4  = addZeroedBitErrorsToCols(input_cntrl, 4);
input_zeroed8  = addZeroedBitErrorsToCols(input_cntrl, 8);
% input_zeroed16 = addZeroedBitErrorsToCols(input_cntrl, 16);
input_random1  = addBitErrorsToCols(input_cntrl, 1);
input_random2  = addBitErrorsToCols(input_cntrl, 2);
input_random4  = addBitErrorsToCols(input_cntrl, 4);
input_random8  = addBitErrorsToCols(input_cntrl, 8);
input_random16 = addBitErrorsToCols(input_cntrl, 16);

% bundle up the input sets into a cell so we can iterate through 
% them nicely
inputSets = cell(0);

% names for all the input sets so we'll have comprehensible output
inputSetNames = cell(0);
if (DEBUG_INPUTS)
    
    inputSets{end+1} = input_cntrl;
    inputSets{end+1} = input_overlap1;
    inputSets{end+1} = input_overlap2;
    inputSets{end+1} = input_zeroed1;
    inputSets{end+1} = input_zeroed2;
    inputSets{end+1} = input_random1;
    inputSets{end+1} = input_random2;

    inputSetNames{end+1} = 'Control';
    inputSetNames{end+1} = 'Overlap1';
    inputSetNames{end+1} = 'Overlap2';
    inputSetNames{end+1} = 'Underlap1';
    inputSetNames{end+1} = 'Underlap2';
    inputSetNames{end+1} = 'Random1';
    inputSetNames{end+1} = 'Random2';

else  % use way more inputs for the real thing

    inputSets{end+1} = input_cntrl;
%     INPUT_SETS{end+1} = input_overlap1;
%     INPUT_SETS{end+1} = input_overlap2;
%     INPUT_SETS{end+1} = input_overlap4;
%     INPUT_SETS{end+1} = input_overlap8;
% %     INPUT_SETS{end+1} = input_overlap16;
%     INPUT_SETS{end+1} = input_zeroed1;
%     INPUT_SETS{end+1} = input_zeroed2;
%     INPUT_SETS{end+1} = input_zeroed4;
%     INPUT_SETS{end+1} = input_zeroed8;
% %     INPUT_SETS{end+1} = input_zeroed16;
    inputSets{end+1} = input_random1;
    inputSets{end+1} = input_random2;
    inputSets{end+1} = input_random4;
    inputSets{end+1} = input_random8;
    inputSets{end+1} = input_random16;

    inputSetNames{end+1} = 'Control';
%     INPUT_SET_NAMES{end+1} = 'Overlap1';
%     INPUT_SET_NAMES{end+1} = 'Overlap2';
%     INPUT_SET_NAMES{end+1} = 'Overlap4';
%     INPUT_SET_NAMES{end+1} = 'Overlap8';
% %     INPUT_SET_NAMES{end+1} = 'input_overlap16';
%     INPUT_SET_NAMES{end+1} = 'Underlap1';
%     INPUT_SET_NAMES{end+1} = 'Underlap2';
%     INPUT_SET_NAMES{end+1} = 'Underlap4';
%     INPUT_SET_NAMES{end+1} = 'Underlap8';
%     INPUT_SET_NAMES{end+1} = 'input_zeroed16';
    inputSetNames{end+1} = 'Random1';
    inputSetNames{end+1} = 'Random2';
    inputSetNames{end+1} = 'Random4';
    inputSetNames{end+1} = 'Random8';
    inputSetNames{end+1} = 'Random16';
end

catStartIdxs = cumsum([1 PATTERNS_A_COUNT PATTERNS_B_COUNT PATTERNS_C_COUNT]);

numInputSets = length(inputSets);
inputObjs = cell(numInputSets, 1);
for i=1:numInputSets
    inputObjs{i} = InputSet(inputSets{i}, inputSetNames{i}, catStartIdxs);
end

end