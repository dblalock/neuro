function [inputObjs] = disjointInputSets(numPatterns, numFeatures, freqs)
% [inputObjs] = disjointInputSets(numPatterns=100, numFeatres=64, freqs=[.1 .2 .3 .4])
%
% Generate an input set with multiple non-overlapping categories, each
% with the specified relative frequencies

SetDefaultValue(1, 'numPatterns', 100);
SetDefaultValue(2, 'numFeatures', 64);
SetDefaultValue(3, 'freqs', [.1 .2 .3 .4]);

NOISE_LEVELS = [0 1 2 4 8];
NUM_NOISE_LEVELS = length(NOISE_LEVELS);
CATEGORIES_COUNT = length(freqs);

if (mod(numFeatures,CATEGORIES_COUNT) ~= 0)
   error('%d categories cant have an equal number of features with %d features',...
       CATEGORIES_COUNT, numFeatures);
end

% determine how many patterns should be in each category
patternCounts = freqs * numPatterns;
if (sum(patternCounts) ~= numPatterns)
   error('Relative frequencies must yield whole numbers of patterns'); 
end

% determine where each category should start and stop
catStopIdxs = cumsum(patternCounts);
catStartIdxs = [1 (catStopIdxs(1:(end-1))+1)];

% generate prototypes for each category
prototypes = zeros(numFeatures, numPatterns);
featuresPerCat = numFeatures / CATEGORIES_COUNT;
oneStartIdxs = 1:featuresPerCat:numFeatures;
warning('off'); % complains because these are doubles and WILL NOT stop
oneStopIdxs = featuresPerCat:featuresPerCat:numFeatures;
for i=1:CATEGORIES_COUNT
    oneStartIdx = oneStartIdxs(i);
    oneStopIdx = oneStopIdxs(i);
    catStartIdx = catStartIdxs(i);
    catStopIdx = catStopIdxs(i);
    prototypes(oneStartIdx:oneStopIdx,catStartIdx:catStopIdx) = 1;
end
% spy(prototypes);


inputObjs = cell(NUM_NOISE_LEVELS,1);
for i = 1:NUM_NOISE_LEVELS
    noise = NOISE_LEVELS(i);
    noisedPatterns = addSwapNoiseToCols(prototypes, noise);
    inputObjs{i} = InputSet(noisedPatterns,['noise',num2str(noise)],catStartIdxs);
end

end