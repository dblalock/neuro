function [] = saveNetsPlot(name)
% utility function so I don't have to keep pasting this

setFigureFontSize(16)
saveas(gcf, ['~/Documents/MATLAB/nets/imgs/',name,'.png']); 
close

end