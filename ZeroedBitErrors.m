function errVect = ZeroedBitErrors(vect, numBits)
% like AddBitErrorsInRange, but automatically just flips bits in the 
% block of ones. If the ones aren't all in a block, this will do bad
% things.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013 Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

vectOnes = find(vect);
errVect = AddBitErrorsInRange(vect,numBits,vectOnes(1), vectOnes(end));

end