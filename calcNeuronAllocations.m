function allocs = calcNeuronAllocations(categoryFreqs, numSynapses)
% allocs = calcNeuronAllocations(categoryFreqs)
%
% Given a vector of relative frequencies for each (disjoint) category,
% determines how many neurons can be expected to have synapses to features
% in that category after a certain number of synapses have been randomly
% selected.

    function [synapseCounts] = evalOrder(order)
        currentCat = -1;
        wSum = -inf;
        synCount = 0;
        for s=1:numSynapses
            category = order(s);
            catFreq = categoryFreqs(category);
            w = getW( catFreq );

            % if another synapse from our current category, just add it on
            if (category == currentCat)
                synCount = synCount + 1;
                wSum = wSum + w;

            % if a synapse from a different category, have it replace our
            % current synapses if its weight is higher than the sum of 
            % all of our current synapses
            elseif w > wSum
                currentCat = category;
                wSum = w;
                synCount = 1;
            end
        end

        % we could write the number of synapses, but I think just a boolean
        % indication of which category won is more meaningful, since this takes
        % into account that more synapses will be added in the future.
        synapseCounts = zeros(1,numCats);
        synapseCounts(currentCat) = 1;
    end

    
    function w = getW(f)
        % returns the weight (variance) for a given relative frequency
        w = f - f*f;
    end

% extract info from args
numCats = length(categoryFreqs);
numOrders = numCats ^ numSynapses;

% initialize vars
allocs = zeros(1,numCats);              % return value
labels = 1:numCats;                     % labels for each category
orders = zeros(numOrders, numSynapses); % possible orderings of categories


% populate a matrix whose rows are possible orders in which features
% from each category could be selected. Categories can be selected 
% more than once, so this is n^d, not n!
for col=1:numSynapses
    
    % this is hard to explain...we're basically writing the same
    % value a whole bunch of times in a row, with the number of times
    % determined by which column we're in. Eg, if this were just 0s and 1s, 
    % everything would get written once in the 1st column, twice in the 2nd 
    % column, 4 times in the 3rd column, etc. This is essentially just a 
    % generalization of that with "numCats" as the branching factor instead
    % of 2.
    stride = numCats^(col-1);
    startIdxs = 1:stride:numOrders;
    stopIdxs = [ (startIdxs(2:end)-1) numOrders];
    blockSize = stride * numCats;
    numBlocks = numOrders/blockSize;
    for offset=0:blockSize:((numBlocks-1)*blockSize)
        for cat = 1:numCats
            startIdx = offset + startIdxs(cat);
            stopIdx = offset + stopIdxs(cat);
            orders(startIdx:stopIdx, col) = labels(cat);
        end
    end
end

% count up how many times each category wins across all the orderings
for i=1:numOrders
    counts = evalOrder(orders(i,:));
    allocs = allocs + counts;
end

% normalize by number of orders calculated
allocs = allocs / numOrders;

end