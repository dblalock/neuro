function [inputObjs] = genInputSets(numPatterns)
% inputObjs = generateInputSets(numPatterns=100)
%
% Generates a series of InputSet objects with different numbers of patterns
% in each category and an amount of noise that varies with input set

SetDefaultValue(1, 'numPatterns', 100);

% define number of categories for input patterns
CATEGORIES_COUNT = 4;

% define number of features per pattern
FEATURES_COUNT = 64;

% fraction of patterns for each category
PATTERNS_A_FRACTION = .1;
PATTERNS_B_FRACTION = .2;
PATTERNS_C_FRACTION = .3;
PATTERNS_D_FRACTION = .4;

% define number of patterns in each category
PATTERNS_A_COUNT = PATTERNS_A_FRACTION * numPatterns;
PATTERNS_B_COUNT = PATTERNS_B_FRACTION * numPatterns;
PATTERNS_C_COUNT = PATTERNS_C_FRACTION * numPatterns;
PATTERNS_D_COUNT = PATTERNS_D_FRACTION * numPatterns;

% constants used to assemble actual input matrices
ZEROS_BLOCK = zeros(FEATURES_COUNT/CATEGORIES_COUNT,1);
ONES_BLOCK  = ones(FEATURES_COUNT/CATEGORIES_COUNT,1);

% prototypes for each category
PROT_A = [ONES_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK];
PROT_B = [ZEROS_BLOCK; ONES_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK];
PROT_C = [ZEROS_BLOCK; ZEROS_BLOCK; ONES_BLOCK; ZEROS_BLOCK];
PROT_D = [ZEROS_BLOCK; ZEROS_BLOCK; ZEROS_BLOCK; ONES_BLOCK];

% matrices for each category--the prototypes replicated the 
% appropriate number of times
PATTERNS_A = repmat(PROT_A, 1, PATTERNS_A_COUNT);
PATTERNS_B = repmat(PROT_B, 1, PATTERNS_B_COUNT);
PATTERNS_C = repmat(PROT_C, 1, PATTERNS_C_COUNT);
PATTERNS_D = repmat(PROT_D, 1, PATTERNS_D_COUNT);

% the actual inputs sets
input_cntrl = [PATTERNS_A PATTERNS_B PATTERNS_C PATTERNS_D];
input_random1  = addSwapNoiseToCols(input_cntrl, 1);
input_random2  = addSwapNoiseToCols(input_cntrl, 2);
input_random4  = addSwapNoiseToCols(input_cntrl, 4);
input_random8  = addSwapNoiseToCols(input_cntrl, 8);
input_random12 = addSwapNoiseToCols(input_cntrl, 12);

% bundle up the input sets into a cell so we can iterate through 
% them nicely; we just add each input to the end of an empty cell 
% so that it's easy to add/remove input sets without having 
% to go back and change exact indices
inputSets = cell(0);

% names for all the input sets so we'll have comprehensible output
inputSetNames = cell(0);

inputSets{end+1} = input_cntrl;
inputSets{end+1} = input_random1;
inputSets{end+1} = input_random2;
inputSets{end+1} = input_random4;
inputSets{end+1} = input_random8;
inputSets{end+1} = input_random12;

inputSetNames{end+1} = 'Noise0';
inputSetNames{end+1} = 'Noise1';
inputSetNames{end+1} = 'Noise2';
inputSetNames{end+1} = 'Noise4';
inputSetNames{end+1} = 'Noise8';
inputSetNames{end+1} = 'Noise12';

% create vector of indices at which each category starts
catStartIdxs = cumsum([1 PATTERNS_A_COUNT PATTERNS_B_COUNT PATTERNS_C_COUNT]);

% bundle up the input patterns and names with the category starts vector
% in the final array of InputSet objects
numInputSets = length(inputSets);
inputObjs = cell(numInputSets, 1);
for i=1:numInputSets
    inputObjs{i} = InputSet(inputSets{i}, inputSetNames{i}, catStartIdxs);
end

end