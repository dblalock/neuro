function [inputObjs] = overlapInputSets(numPatterns)
% inputObjs = generateInputSets(numPatterns=100)
%
% Generates a series of InputSet objects with different numbers of patterns
% in each category and an amount of noise that varies with input set

SetDefaultValue(1, 'numPatterns', 100);

NUM_FEATURES = 64;
% patternFractions = [.04 .04 .04 .04 .04 .04 .04 .04];
PATTERN_FRACTIONS = zeros(1,8) + .08;
OVERLAP_FRACTIONS = [0 0 .25 0 .5 0 .75];   % pairs orthogonal, with 0:.25:.75 overlap within pairs

% determine how many patterns are in each category, how many categories,
% and how many features are in each pattern
patternCounts = numPatterns * PATTERN_FRACTIONS;
numCats = length(patternCounts);
featuresPerCat = NUM_FEATURES / numCats;

% initialize patterns matrix for prototype input set
patterns = zeros(NUM_FEATURES, numPatterns);

% initialize array of input set objects
maxNoise = ceil(featuresPerCat/2);
numNoiseAmts = ceil(log2(maxNoise)) + 1;
noiseAmts = 2 .^ (0:(numNoiseAmts -1) );
inputObjs = cell(numNoiseAmts + 1, 1);

% -------------------------------
% determine the pattern indices at which categories start and stop
% -------------------------------

% determine start and stop indices without overlap
catStopIdxs = cumsum(patternCounts);
catStartIdxs = [1 (catStopIdxs(1:(end-1)) +1)];

% determine amount of overlap
offsets = zeros(1,numCats);
for i=2:numCats
   avgLen = (patternCounts(i-1) + patternCounts(i) )/2;
   offsets(i) = round(avgLen * OVERLAP_FRACTIONS(i-1));
end

% account for overlap
offsets = cumsum(offsets);
catStopIdxs = catStopIdxs - offsets;
catStartIdxs = catStartIdxs - offsets;

% create start idxs that will be used for evaluating categories; at
% present, these consist of every other start idx, since the categories
% grouped in overlapping pairs, and it's the resources allocated to the pair
% that we care about
pairStartIdxs = [catStartIdxs(1:2:numCats) (catStopIdxs(end)+1)];

% -------------------------------
% generate prototype input set
% -------------------------------

% determine the rows at which the features must start and stop for each
% category
featureStopIdxs = zeros(1,numCats) + featuresPerCat;
featureStopIdxs = cumsum(featureStopIdxs);
featureStartIdxs = [1 (featureStopIdxs(1:(end-1)) +1)];

% set the appropriate block of features within the appropriate block
% of patterns to 1
for i=1:numCats
    catStartIdx = catStartIdxs(i);
    catStopIdx = catStopIdxs(i);
    featureStartIdx = featureStartIdxs(i);
    featureStopIdx = featureStopIdxs(i);
    patterns(featureStartIdx:featureStopIdx, catStartIdx:catStopIdx) = 1;
end

% spyMat(patterns, '','','')

% add garbage with the same number of features at the end
garbageStart = catStopIdxs(end) + 1;
garbageStop = numPatterns;
patterns(:, garbageStart:garbageStop) = ...
    addBitErrorsToCols(patterns(:, garbageStart:garbageStop), featuresPerCat);

% spyMat(patterns, '','','')


inputObjs{1} = InputSet(patterns, 'Noise0', pairStartIdxs);

% -------------------------------
% generate noisy input sets
% -------------------------------

for i=1:length(noiseAmts)
    noiseAmt = noiseAmts(i);
    noisePatterns = addSwapNoiseToCols(patterns, noiseAmt);
    name = ['Noise',num2str(noiseAmt)];
    inputObjs{i+1} = InputSet(noisePatterns, name, pairStartIdxs);
end

% for i=1:length(inputObjs)
%    spyInput(inputObjs{i}) 
% end

end