function makePubFigs()
% make figs for the pub

NUM_INPUT_PATTERNS = 100;
NUM_INPUT_FEATURES = 80;
CATEG_FREQS = [.06 .13 .2 .27 .34];

INPUT_SET_STABLE = 1;
INPUT_SET_UNSTABLE = 5;
% LOGS_FILE = ['pubFigsLogs_inputSet',num2str(INPUT_SET_TO_USE),'.mat'];
LOGS_FILE_STABLE = ['pubFigsLogs_inputSetStable',num2str(INPUT_SET_STABLE),'.mat'];
LOGS_FILE_UNSTABLE = ['pubFigsLogs_inputSetUnstable',num2str(INPUT_SET_UNSTABLE),'.mat'];

LOAD = 1;
SAVE = 0;

inputSets = disjointInputSets(NUM_INPUT_PATTERNS, NUM_INPUT_FEATURES, CATEG_FREQS);

p = NetworkParams();
p.neuronCount = 40;
p.synaptogenKickstartRounds = 10;
p.synaptogenRoundsLimit = 2000;

p.wNewConn = .2;
p.wRateConst = .01;
p.threshold = 1;
p.log = 1;
p.verbose = 1;

if (LOAD)
%     logsFileStable = LOGS_FILE;
%     logsFileUnstable = LOGS_FILE;
    fileStable = load(LOGS_FILE_STABLE);
    fileUnstable = load(LOGS_FILE_UNSTABLE);
    Lstable = fileStable.Lstable;
    Lunstable = fileUnstable.Lunstable;
elseif SAVE
    p.minFiringRate = .05;
    [~, Lstable] = createNetwork(inputSets{INPUT_SET_STABLE}, p);
    p.minFiringRate = .15;
    [~, Lunstable] = createNetwork(inputSets{INPUT_SET_UNSTABLE}, p);
    save(LOGS_FILE_STABLE, 'Lstable');
    save(LOGS_FILE_UNSTABLE, 'Lunstable');
end

disp(Lstable)
disp(Lunstable)

synapseCountsStable = cell2mat(Lstable.synapses_individual_log)';
subplot(2,1,1), plot(synapseCountsStable, 'LineWidth',1.25)
title('Number of synapses for each neuron across simulation')
ylabel('Number of Synapses')
synapseCountsUnstable = cell2mat(Lunstable.synapses_individual_log)';
subplot(2,1,2), plot(synapseCountsUnstable, 'LineWidth',1.25)
ylabel('Number of Synapses')
xlabel('Time step')
setFigureFontSize(17)
saveNetsPlot('figs/stabilityComparison')

end