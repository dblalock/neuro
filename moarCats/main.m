function main()

NUM_RUNS = 6;       % number of networks to avg together
LOAD_DATA = 1;      % don't recreate networks?
LOAD_METRICS = 1;   % don't reanalyze networks?

PLOT_INPUT_SETS = 0;
PLOT_INPUT_EUC_DISTS = 0;
DISP_INP_VS_OUTPUT_STATS = 0;

NUM_INPUT_PATTERNS = 100;
NUM_INPUT_FEATURES = 80;
NUM_TEST_PATTERNS = 1000;

CATEG_FREQS = [.06 .13 .2 .27 .34];

% threshold values to sweep
% THRESHS = [.5 1 1.5 2];
% THRESHS = [1 1.5 2];
% THRESHS = [1.5 2];
% THRESHS = [2];
THRESHS = [];
% THRESHS = [THRESHS .5];
THRESHS = [THRESHS 1];
% THRESHS = [THRESHS 1.5];
% THRESHS = [THRESHS 2];

FIRING_RATES = .05:.05:.2;

% generate input sets using our custom function
inputSets = disjointInputSets(NUM_INPUT_PATTERNS, NUM_INPUT_FEATURES, CATEG_FREQS);
numInputSets = numel(inputSets);

% disp(patternSetStats(inputSets{1}.inputPatterns, inputSets{1}.catStartIdxs) )

% generate input sets using our custom function
testSets = disjointInputSets(NUM_TEST_PATTERNS, NUM_INPUT_FEATURES, CATEG_FREQS);

% testSets{1}.inputPatterns
% return

% create a spy diagram for each input set
if (PLOT_INPUT_SETS)
    for i=1:numInputSets
        inputSet = inputSets{i};
        spyInput(inputSet)
        saveNetsPlot(['figs/inputs/',inputSet.name])
    end
end

% create a histogram of all-pairs euclidean distances for each input set
if PLOT_INPUT_EUC_DISTS
    dirName = 'figs/inputs/';
    for i=1:numInputSets
        inputSet = inputSets{i};
        patterns = inputSet.inputPatterns;
        cStartIdxs = inputSet.catStartIdxs;
        name = ['eucDist_',inputSet.name];
        euclideanDistsHist(patterns, cStartIdxs, name, dirName);
    end
end

% generate parameters for network construction
p = NetworkParams();
p.neuronCount = 40;
p.synaptogenKickstartRounds = 10;
p.synaptogenRoundsLimit = 2000;
p.minFiringRate = .05;
p.wNewConn = .2;
p.wRateConst = .01;
p.threshold = 1;
p.log = 0;
p.verbose = 0;

% initialize container for network metrics across sweep
allAvgMetrics = cell(numInputSets, length(THRESHS) );

% sweep threshold
tStart = tic();
% for i=1:numel(THRESHS)
%     thresh=THRESHS(i);
%     p.threshold = thresh;
for i=1:numel(FIRING_RATES)
    minFiring = FIRING_RATES(i);
    p.minFiringRate = minFiring;
    fprintf('Testing Firing Rate: %.2f\n',minFiring);

    % determine file name to save analyses as
    fileName = generateFileName('', p);
    metricsFileName = [fileName,'_metrics.mat'];
    
    % run the simulations and get the averaged results;
    % or, alternatively, just load old analyses
    if (LOAD_METRICS)
        file = load(metricsFileName);
        avgMetrics = file.avgMetrics;
        runMetrics = file.runMetrics;
    else
        [avgMetrics runMetrics] = simulate(inputSets, testSets, p, NUM_RUNS, LOAD_DATA);
        save(metricsFileName, 'avgMetrics', 'runMetrics');
    end
    
    % display some random results as an example %TODO remove hack
    if DISP_INP_VS_OUTPUT_STATS
        metrics = runMetrics{2,1};
        fprintf('Minimum activity present: %.3f\n',metrics.minAct)
        disp( metrics.inputSetStats)
        disp( metrics.outputSetStats)
    end
    
    % display the results
    for j=1:numel(avgMetrics)
        metrics = avgMetrics{j};
        name = inputSets{j}.name;
        fprintf('Input Set: %s\n', name);
        dirName = ['figs/firing',num2str(minFiring*1000)];
%         dirName = ['figs/thresh',num2str(thresh*1000)];
%         visualizeNetwork(metrics, name, dirName);
        close all
    end
    
    % store the average metrics; we use deal() to copy
    % values over in a vectorized way
    [allAvgMetrics{:, i}] = deal(avgMetrics{:});

end
toc(tStart)

return

noiseBits = [0 1 2 4 8]; % property of input sets
THRESHS = FIRING_RATES; % hack to make it plot vs firing rate

% display classification mutual information across noise + threshold
imgStructs(allAvgMetrics, 'classificationInfo', THRESHS, noiseBits)
labelFigure('Mutual information between input and output classifications', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/classificationInfo')

% display classification mutual information across noise + threshold
imgStructs(allAvgMetrics, 'classificationBitsPerJ', THRESHS, noiseBits)
labelFigure('Bits per Joule for input and output classifications', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/classificationBitsPerJ')

% display mean activity level across noise + threshold
imgStructs(allAvgMetrics, 'meanAct', THRESHS, noiseBits)
labelFigure('Mean neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanAct')

% display min activity level across noise + threshold
imgStructs(allAvgMetrics, 'minAct', THRESHS, noiseBits)
labelFigure('Minimum neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/minAct')

% display max activity level across noise + threshold
imgStructs(allAvgMetrics, 'maxAct', THRESHS, noiseBits)
labelFigure('Maximum neuron activity for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/maxAct')

% display  across noise + threshold
imgStructs(allAvgMetrics, 'nonzeroNeurons', THRESHS, noiseBits)
labelFigure('Number of nonzero neurons for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/nonzeroNeurons')

% display statistical dependence across noise + threshold
imgStructs(allAvgMetrics, 'statDep', THRESHS, noiseBits)
labelFigure('Statistical Dependence for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/statDep')

% display mean cost across noise + threshold
imgStructs(allAvgMetrics, 'meanCost', THRESHS, noiseBits)
labelFigure('Mean energy cost for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanCost')

% display mean cost per neuron across noise + threshold
imgStructs(allAvgMetrics, 'meanCostPerNeuron', THRESHS, noiseBits)
labelFigure('Mean energy cost per neuron for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/meanCost')

% display mutual information across noise + threshold
imgStructs(allAvgMetrics, 'mutualInfo', THRESHS, noiseBits)
labelFigure('Mutual Information for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/mutualInfo')

% display bits per joule across noise + threshold
imgStructs(allAvgMetrics, 'bitsPerJ', THRESHS, noiseBits)
labelFigure('Bits Per Joule for different network parameters', ...
    'Threshold', 'Bits Swapped')
saveNetsPlot('figs/BPJ')

end