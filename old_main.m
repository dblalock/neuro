function [] = main() 

% ================================================================
% CONSTANTS
% ================================================================

% -------------------------------
% network structure
% -------------------------------

NEURON_COUNT = 40;

% -------------------------------
% constants pertaining to code execution
% -------------------------------

NUM_RUNS = 1;           % How many times to test each set of parameters
SAVE_DATA = 1;          % Actually run the simulation and save data
LOAD_DATA = 0;          % If we didn't run, load data and analyze that
ONE_RUN = 0;            % For debugging; just construct one network
RECORD_HISTORIES = 1;   % Record statistics over time
PRINT_INCREMENTAL = 0;  % For debugging; every 10 rounds of synaptogen, prints stuff
PLOT = 1;               % Creates figures for numerous network metrics
SPY_NETWORK = 0;        % Creates a spy diagram of the final set of connections
DISP_INPUT_STATS = 0;   % Displays statistics about our input set and exits
PLOT_INPUTS = 0;        % Creates figures illustrating our input sets
ENSURE_STABILITY = 1;   % Uses a more rigorous stability metric
FIRST_INPUT = 1;        % Start with the nth input set in our list, where n is this number
CALL_ANALYZE_NETWORK=1;	% Call our revised network analysis function instead of final project code

if (NUM_RUNS > 1)   % doesn't make sense to average variable-length histories
    RECORD_HISTORIES = 0;
end

% -------------------------------
% input data
% -------------------------------

[INPUT_SETS INPUT_SET_NAMES FEATURES_COUNT PATTERNS_COUNT CATEGORY_START_INDICES] = generateInputSets();
% [INPUT_SETS INPUT_SET_NAMES FEATURES_COUNT PATTERNS_COUNT CATEGORY_START_INDICES] = loadInputSets('digits.dat');

CATEGORIES_COUNT = length(CATEGORY_START_INDICES);
INPUT_SETS_COUNT = length(INPUT_SETS);

% start at an input other than the first
if (FIRST_INPUT > 1)
    INPUT_SETS(1:(FIRST_INPUT-1) ) = [];
    INPUT_SET_NAMES(1:(FIRST_INPUT-1) ) = [];
    INPUT_SETS_COUNT = length(INPUT_SETS);
end

if (DISP_INPUT_STATS)
    numIters = 50;
    inputSetsCopies = cell(numIters);
    for i=1:numIters
        inputSetsCopies{i} = generateInputSets();
    end
    
    fprintf('input\tinfo\tdep\tuniq\toverlap\n')
    
    protos = INPUT_SETS{1};
    for i=1:INPUT_SETS_COUNT
        input_name = INPUT_SET_NAMES{i};
        info = 0;
        dep = 0;
        uniq = 0;
        overlap = 0;
        for j=1:numIters
            inputSets = inputSetsCopies{j};
            input = inputSets{i};
            [repInfo uniqColCounts] = columnEntropy(input);
            overlapPts = (input - protos) > 0;
            
            info = info + repInfo;
            dep = dep + statisticalDependence(input);
            uniq = uniq + length(uniqColCounts);
            overlap = overlap + sum(sum(overlapPts));
        end
        avgInfo = info / numIters;
        avgDep = dep / numIters;
        avgUniq = uniq / numIters;
        avgOverlap = overlap / (numIters * PATTERNS_COUNT);
        
        fprintf('%s\t%.2f\t%.2f\t%.2f\t%.2f\n', input_name, avgInfo, ...
            avgDep, avgUniq, avgOverlap);
    end
    return
end

if(PLOT_INPUTS)
    saveDir = 'proj3Inputs/';
    mkdir(saveDir)
    for i=1:INPUT_SETS_COUNT
        input = INPUT_SETS{i};
        inputName = INPUT_SET_NAMES{i};
        spyNet(input, 'Pattern Number', 'Index', inputName);
        saveNetsPlot([saveDir,inputName])
    end
    return
end


% -------------------------------
% constants pertaining to synaptogenesis/shedding
% -------------------------------

% RECEPTIVITY_EXPONENT = 12;  % exponent used in receptivity formula
% RECEPTIVITY_POLE_POS = 1;  % coefficient for desired min firing level used 
                            % in receptivity calculation
RECEPTIVITY_SCALE_FACTOR = .01;   % scale factor to prevent too many connections early on
                                   % NOTE: was originally .005 for
                                   % reasonable numbers; it's cranked up a
                                   % lot at the moment
% RECEP_CUTOFF = .1*RECEPTIVITY_SCALE_FACTOR;

% constants for average firing rate
ACTIVITY_RATE_CONST = .01;

% minimum firing rates to test
MIN_FIRING_RATES = [.08 .18];%[.2 .1 .01];%[.01 .1];

% iterations through input set between rounds of synaptogenesis
INTER_SYNAPTOGEN_ITERATIONS = 10;

% number of rounds of synaptogenesis to create initial random network
SYNAPTOGEN_KICKSTART_ROUNDS = 0;

% -------------------------------
% constants pertaining to threshold and weight modification
% -------------------------------
W_NEW_CONN = .4;    % not taken from paper - weight of newly created connection
W_RATE_CONST = .025;
W_CUTOFF = .025;
THRESH_VALS = [.6 1];%[1 2 3];

% -------------------------------
% constants pertaining to energy costs
% -------------------------------
% COST_SYNAPSE_USED = 1;
% COST_SYNAPSE_UNUSED = 1/15;
% COST_FIRE_0 = .75;
% COST_FIRE_1 = 3.75;

% -------------------------------
% constants defining termination of network construction
% -------------------------------
if (ONE_RUN)
    DELTA_SYNAPSES_FINISH_THRESH = .1;
else
    DELTA_SYNAPSES_FINISH_THRESH = .01;
end
DELTA_SYNAPSES_RATE_CONST = .01;

SYNAPTOGEN_ROUNDS_LIMIT = 5000;

% -------------------------------
% constants pertaining to logging
% -------------------------------

% ================================================================
% INTIALIZATION
% ================================================================

% initialize containers for our analytics data
final_weights = containers.Map();
network_metrics_list = cell(0);

infos = zeros(1, INPUT_SETS_COUNT);
repCaps = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT);
neurAllocs = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT);
statDeps = zeros(1, INPUT_SETS_COUNT);


% ================================================================
% MAIN LOOP
% ================================================================
if(SAVE_DATA || LOAD_DATA)
tic;
for input_set_num=1:INPUT_SETS_COUNT
    input_set_name = INPUT_SET_NAMES{input_set_num};
    
    disp(' ')
    disp('================================================================')
    fprintf('input set: %s\n', input_set_name );
    disp('================================================================')

    % train on the appropriate input set for this iteration
    input_pool = INPUT_SETS{input_set_num};
    
    % expected value of input for our dW rule
    E_x = mean(input_pool, 2);
    E_x = repColVectFast(E_x, NEURON_COUNT);
    
    for min_firing=MIN_FIRING_RATES
        disp(' ')
        disp('--------------------------------------------------------------')
        fprintf('min firing rate: %.2f\n', min_firing);
        disp('--------------------------------------------------------------')
    
        % calculate receptivity constants for this firing rate
%         recep_const = (min_firing*RECEPTIVITY_POLE_POS) .^ RECEPTIVITY_EXPONENT;
%         recep_numerator = recep_const * RECEPTIVITY_SCALE_FACTOR;
        
        % for each firing rate, test out a number of different thresholds
        for thresh=THRESH_VALS
            if (length(THRESH_VALS) > 1)
                disp(' ')
                disp('-------------------------------')
                fprintf('thresh: %.1f\n', thresh);
                disp('-------------------------------')
            end
            % don't bother with any of the network construction or logging
            % if we're just going to load our data from a previous run
            if (~LOAD_DATA)
            
            % initialize averages across multiple runs
            info_avg = 0;
            repCapacities_avg = 0;
            neuronAllocs_avg = 0;
            statDep_avg = 0;
            
            for run_number=1:NUM_RUNS
            
            % -------------------------------
            % initialize logs
            % -------------------------------
                
            % we don't know how many
            % rounds of synaptogenesis we'll need, so we can't preallocate
            % space very precisely
            activation_log = cell(1);    % don't know length, so use cell
            synaptogen_log = cell(1);
            synaptoshed_log = cell(1);
            synapses_log = cell(1);
            synapses_individual_log = cell(1);
            receps_individual_log = cell(1);
%             cost_log = zeros(PATTERNS_COUNT,1); % can preallocate cuz just 
                            % tracking most recent iteration thru patterns
                            
            % -------------------------------
            % initialize weights
            % -------------------------------
            
            % initialize weight matrix to zeros; since we don't anticipate
            % that many connections, make it a sparse matrix
            %round(NEURON_COUNT * thresh * 10);
            estimatedConnections = 13;  % empirically derived
            W = spalloc(FEATURES_COUNT, NEURON_COUNT, estimatedConnections);  
            
            % -------------------------------
            % network construction
            % -------------------------------
            synaptogen_round = 0;
            networkFinished = 0;
            delta_synapses = 5;     % has to be > 0 so won't terminate prematurely
            avgActivation = zeros(1, NEURON_COUNT);
            while(~networkFinished)
                
                % -------------------------------
                % synaptogenesis
                % -------------------------------

                synaptogen_round = synaptogen_round + 1;
                
                % exponential receptivity rule
%                 recep_vect = (ones(1,NEURON_COUNT) * recep_numerator) ./ ...
%                     (recep_const + avgActivation .^ RECEPTIVITY_EXPONENT);
%                 recep_vect = recep_vect .* (recep_vect > RECEP_CUTOFF);
                
                % rectangular receptivity rule
                recep_vect = RECEPTIVITY_SCALE_FACTOR * (avgActivation < min_firing);
                recep_matrix = repRowVectFast(recep_vect, FEATURES_COUNT);
                
                new_connections = rand(FEATURES_COUNT, NEURON_COUNT) < recep_matrix;
                new_connections = sparse(new_connections);  % had BETTER be sparse...
                new_connections = new_connections - and(new_connections, W);  % don't add duplicates
                new_connections = new_connections .* W_NEW_CONN;
                
                W = W + new_connections;
                
                % start with a number of random connections to speed the
                % process
                if (synaptogen_round < SYNAPTOGEN_KICKSTART_ROUNDS) continue; end;
                
                % if not enough connections yet to possibly fire,
                % just do more synaptogenesis
                weight_sums = sum(W);
                if (max(weight_sums) < thresh) continue; end
                
                % -------------------------------
                % train the network with the current set of connections
                % -------------------------------
                
                for iteration=1:INTER_SYNAPTOGEN_ITERATIONS
                    
                    % train on each input pattern, taken in random order
                    for i=randperm(PATTERNS_COUNT)
                        input_pattern = input_pool(:,i);
                        
                        % calculate excitation
                        W_mask = W > W_CUTOFF;
                        W_excite = W .* W_mask;
                        Y = input_pattern' * W_excite;
                       
                        % calculate and record activation
                        Z = Y > thresh;
                        avgActivation = (1-ACTIVITY_RATE_CONST)*avgActivation ...
                            + ACTIVITY_RATE_CONST*Z;
                       
                        % modify synaptic weights
                        X = repColVectFast(input_pattern, NEURON_COUNT);
                        Y = repRowVectFast(Y, FEATURES_COUNT) .* W_mask;
                        dW = W_RATE_CONST .* Y .* (X - W - E_x);
%                         dW = W_RATE_CONST .* Y .* (X - W);
                        W = W + dW;
                    
                    end % run through input patterns
                    
                end % inter-synaptogenesis training cycles
                
                % -------------------------------
                % logging
                % -------------------------------
                
                W_above_cutoff = (W >= W_CUTOFF);
                individual_synapse_counts = sum(W_above_cutoff);
                synapses_count = sum(individual_synapse_counts);
                synapses_count = full(synapses_count);  % un-sparse so normal int
                synaptogen_count = sum(sum(new_connections > 0) );
                synaptoshed_count = length(nonzeros(W)) - synapses_count;
                
                if (PRINT_INCREMENTAL && (mod(synaptogen_round, 10) == 0) )
                    rounds = synaptogen_round
                    synapses = synapses_count
                    avg_delta_synapses = delta_synapses
                    activities = avgActivation(1:8) % subset that'll print 
                    receptivities = recep_vect(1:8) % on one line
                end
                
                if (RECORD_HISTORIES)
                    activation_log{end+1} = mean(avgActivation);
                    synaptogen_log{end+1} = synaptogen_count;
                    synapses_log{end+1} = synapses_count;
                    synapses_individual_log{end+1} = individual_synapse_counts';
                    receps_individual_log{end+1} = recep_vect';
                    synaptoshed_log{end+1} = synaptoshed_count;
                end
                
                % -------------------------------
                % synaptic shedding
                % -------------------------------

                W = W .* W_above_cutoff;
                
                % -------------------------------
                % termination test
                % -------------------------------
                
                synapseCountChange = (synaptogen_count - synaptoshed_count);
                delta_synapses = (1-DELTA_SYNAPSES_RATE_CONST) * delta_synapses ...
                    + DELTA_SYNAPSES_RATE_CONST * synapseCountChange;

                % end run all neurons above min firing rate and network stable
                if (ENSURE_STABILITY)
                    networkFinished = all(avgActivation > min_firing) && ...
                        delta_synapses < DELTA_SYNAPSES_FINISH_THRESH;
                else
                    networkFinished = all(avgActivation > min_firing);
                end
                
                if (synaptogen_round >= SYNAPTOGEN_ROUNDS_LIMIT)
                    networkFinished = 1;
                end
                
            end % network construction
            
            fprintf('rounds synaptogen, synapses: %d, %d\n', ...
                synaptogen_round, synapses_count);
            
            % -------------------------------
            % compute running totals across runs
            % -------------------------------
            
            excitation = (input_pool' * W);
            output = excitation > thresh;
            
            output_entropy = columnEntropy(output');
            neuronAllocs = neuronAllocations(output, CATEGORY_START_INDICES);
            repCapacities = representationalCapacities(output, CATEGORY_START_INDICES);
            statDep = statisticalDependence(output);
            
            info_avg = info_avg + output_entropy;
            neuronAllocs_avg = neuronAllocs_avg + neuronAllocs;
            repCapacities_avg = repCapacities_avg + repCapacities;
            statDep_avg = statDep_avg + statDep;

            % -------------------------------
            % save weights and average activations (we can calculate
            % everything else later from just these)
            % -------------------------------
            if (SAVE_DATA)
                paramStr = sprintf('%s_fire%d_thresh%d_run%d', ...
                    input_set_name, min_firing*1000,thresh*1000, run_number);
                saveFile = [paramStr,'.mat'];
                save(saveFile, 'W', 'avgActivation');
            end
            
            end % individual run
            
            % rename vars as if they came from an individual run for
            % compatibility
%             W = W_avg;
%             avgActivation = activ_avg;
            
            % -------------------------------
            % compute averages across runs
            % -------------------------------
            output_entropy = info_avg ./ NUM_RUNS
            neuronAllocs = neuronAllocs_avg ./ NUM_RUNS
            repCapacities = repCapacities_avg ./ NUM_RUNS
            statDep = statDep_avg ./ NUM_RUNS
            
            if (SAVE_DATA)
                saveFile = sprintf('%s_fire%d_thresh%d_infoStuff.mat', ...
                    input_set_name, min_firing*1000,thresh*1000);
                save(saveFile, 'output_entropy', 'neuronAllocs', ...
                    'repCapacities', 'statDep');
            end
            
            end % if ~LOAD_DATA
            
            % -------------------------------
            % save/load data
            % -------------------------------
            
            paramStr = sprintf('%s_fire%d_thresh%d', ...
                input_set_name, min_firing*1000,thresh*1000);
            saveFile = [paramStr,'.mat'];
            if (LOAD_DATA)
                d = load(saveFile);
                avgActivation = d.avgActivation;
                W = d.W;
                input_pool = d.input_pool;
                if (RECORD_HISTORIES)
                    synaptogen_log = d.synaptogen_log;
                    synapses_log = d.synapses_log;
                    synapses_individual_log = d.synapses_individual_log;
                    synaptoshed_log = d.synaptoshed_log;
                    activation_log = d.activation_log;
                    recep_vect = d.recep_vect;
                    receps_individual_log = d.receps_individual_log;
                end
            elseif (SAVE_DATA)
                save(saveFile,'avgActivation','synaptogen_log', ...
                    'synapses_log', 'synaptoshed_log', 'activation_log', ...
                    'W','input_pool', 'recep_vect', ...
                    'synapses_individual_log', 'receps_individual_log');
                final_weights(paramStr) = W;
            end
            
            % -------------------------------
            % network analytics (entropies, activity levels, etc)
            % -------------------------------
            
			
			% NEW FUNCTION TO JUST DO WHAT WE CARE ABOUT
			if (CALL_ANALYZE_NETWORK)
				analyzeNetwork(W, thresh, min_firing, ...
					input_pool, input_set_name, CATEGORY_START_INDICES);
				continue
			end
			
            % create a data structure to store everything
            network_metrics = struct('W', W);
            network_metrics.name = input_set_name;
            
            % W = FEATURES_COUNT x NEURON_COUNT
            % input = FEATURES_COUNT x PATTERNS_COUNT
            % output = FEATURES_COUNT x NEURON_COUNT
            excitation = (input_pool' * W);
            output = excitation > thresh;
            
            % activity
%             activs = mean(output);  % true avg across input set
            activs = avgActivation;     % final moving avg
            minAct  = min(activs);
            maxAct  = max(activs);
            stdAct  = std(activs);
            meanAct = mean(activs);
            network_metrics.activs  = activs;
            network_metrics.minAct  = minAct;
            network_metrics.maxAct  = maxAct;
            network_metrics.stdAct  = stdAct;
            network_metrics.meanAct = meanAct;

            % energy cost
            synapses_total = sum(sum( W > 0 ) );
            synapses_used  = sum(sum( W > W_CUTOFF) );
            synapseCosts = synapses_used * ...
                (COST_SYNAPSE_USED - COST_SYNAPSE_UNUSED) ...
                + COST_SYNAPSE_UNUSED * synapses_total;

            firing_ones = sum(sum(output));
            firingCosts = firing_ones * (COST_FIRE_1 - COST_FIRE_0) ...
                + COST_FIRE_0 * NEURON_COUNT;

            totalCost = synapseCosts + firingCosts;
            meanCost = totalCost / PATTERNS_COUNT;
            meanCostPerNeuron = meanCost / NEURON_COUNT;
            network_metrics.totalCost = totalCost;
            network_metrics.meanCost = meanCost;
            network_metrics.meanCostPerNeuron = meanCostPerNeuron;

            % output entropy
            [output_entropy symbol_counts uniq_outputs] = columnEntropy(output');
            network_metrics.output_entropy = output_entropy;
            
            % number of codewords
            codeWordCounts = sort(symbol_counts, 'descend');
            network_metrics.codeWordCounts = codeWordCounts;
            
            % bits per Joule
            bitsPerJ = output_entropy / meanCost;
            network_metrics.bitsPerJ = bitsPerJ;
            
            % output for table
%             disp(sprintf('meanAct\tstdAct\tminAct\tmaxAct\toutput_entropy\tbitsPerJ'))
            metrics = [meanAct stdAct minAct maxAct output_entropy bitsPerJ];
            network_metrics.metrics = metrics;
            
            % visualize the final state of the network
            if (SPY_NETWORK)
                spyNet(input_pool, 'Pattern Number', 'Index', 'Input Pool')
                spyNet(W, 'Neuron Number', 'Input Neuron', 'W')
                spyNet(output, 'Neuron Number', 'Input Pattern', 'Output')
            end
            
            % display the fancy metrics
            info = output_entropy
            repCapacities = representationalCapacities(output, CATEGORY_START_INDICES)
            neuronAllocs = neuronAllocations(output, CATEGORY_START_INDICES)
            statDep = statisticalDependence(output)
            network_metrics.repCapacities = repCapacities;
            network_metrics.neuronAllocs = neuronAllocs;
            network_metrics.statDep = statDep;
            
            network_metrics_list{end+1} = network_metrics;
            
            % -------------------------------
            % visualizations of everything ever
            % -------------------------------
            
            if (PLOT && RECORD_HISTORIES)
                % make directory in which to save plots
                saveDir = sprintf('projOut/%s/fire%d/thresh%d/', ...
                    input_set_name, min_firing*1000,thresh*1000);
                mkdir(['~/Documents/MATLAB/nets/imgs/',saveDir]);
                
                % make a utility function to save figures in the right dir
                saveFig = @(figName) saveNetsPlot([saveDir,figName]);
                
                % activation history plot
                activation_history = cell2mat(activation_log);
                skeletonPlot(1:length(activation_history), activation_history, ...
                    'Average Activation vs Syntaptogenesis Cycle', ...
                    'cycle', 'Activation', 'b-');
                axis auto
                saveFig('activation_hist')
                
                % individual synapse counts plot
                % 
                % Commented out because it's too much data--it overflows
                % the heap and doesn't actually plot if the network takes
                % too long to become stable
                % 
% %                 individual_synapses_history = cell2mat(synapses_individual_log);
% %                 skeletonPlot(1:length(individual_synapses_history), ...
% %                     individual_synapses_history(1,:), ...
% %                     'Synapse Counts for Individual Neurons vs Syntaptogenesis Cycle', ...
% %                     'cycle', 'Synapses', 'b-');
% %                 hold all
% %                 for i=2:5:NEURON_COUNT
% %                     plot(individual_synapses_history(i,:));
% %                 end
% %                 saveFig('individual_synapses_hist')
% %                 hold off
                
                % overall synapse count plot
                synapses_history = cell2mat(synapses_log);
                skeletonPlot(1:length(synapses_history), synapses_history, ...
                    'Total Synapses vs Syntaptogenesis Cycle', ...
                    'cycle', 'Synapses in network', 'b-');
                saveFig('synapses_hist')
                
                % cosine comparison of output symbols
                cosineComparison = cosineCompare(output');
                imagesc(cosineComparison)
                labelFigure('Cosine Comparison', 'output symbol index',...
                    'output symbol index');
                colorbar
                saveFig('cosineComparison')
                
                
                % histogram of activity variances for final run
                activityVariances = var(output);
                hist(activityVariances)
                labelFigure('Variance in firing rate across neurons',...
                    'Variance in firing rate', 'Number of neurons');
                saveFig('actVariances')
                
                % histogram of mean activity for final run
                activityMeans = mean(output);
                hist(activityMeans,20)
                labelFigure('Mean firing rate across neurons',...
                    'Mean firing rate', 'Number of neurons');
                saveFig('actMeans')

                % histogram of mean excitation for final run
                excitationMeans = mean(excitation);
                hist(excitationMeans, 20)
                labelFigure('Mean excitation across neurons',...
                    'Mean excitation', 'Number of neurons');
                saveFig('exciteMeans')

                % histogram of number of synapses for final run
                synapses = sum(W>0);
                hist(synapses)
                labelFigure('Number of synapses across neurons',...
                    'Number of synapses', 'Number of neurons');
                saveFig('synapseCounts')

                % histogram of weights across all neurons after final run
                weights = sum(W);
                hist(weights,20)
                labelFigure('Frequency of total weight values across neurons',...
                    'Sum of synaptic weights', 'Number of neurons');
                saveFig('weightsSums')

                % receptivity scatter plot after network stabilizaes
                scatter(activs, recep_vect, 100, 'k')
                axis([0 1 0 RECEPTIVITY_SCALE_FACTOR] )
                labelFigure('Receptivity vs Average Firing rate',...
                    'Avg firing rate', 'Receptivity');
                saveFig('receptivityVsActivity')
                
                % receptivities for individual neurons across time
                individual_receps_history = cell2mat(receps_individual_log);
                skeletonPlot(1:length(individual_receps_history), ...
                    individual_receps_history(1,:), ...
                    'Receptivities for Individual Neurons vs Syntaptogenesis Cycle', ...
                    'cycle', 'Receptivity', 'b-');
                axis auto
                hold all
                for i=2:5:NEURON_COUNT
                    plot(individual_receps_history(i,:));
                end
                saveFig('individual_synapses_hist')
                hold off
            end
            
            % stop after one network for testing
            if (ONE_RUN)
                toc
                save('final_weights.mat', 'final_weights');
                save('network_metrics_list.mat', 'network_metrics_list')
                return 
            end
            
        end % threshold
    end % min firing rate
end % input set
toc;

if (SAVE_DATA)
    save('final_weights.mat', 'final_weights');
    save('network_metrics_list.mat', 'network_metrics_list')
end

end


if CALL_ANALYZE_NETWORK
	return
end

% -------------------------------
% A whole bunch of operations for my final project. These had to
% get done in a hurry, so it's all just kind of thrown in here. I'm
% not 100% sure how it all works anymore.
%
% The basic idea is calculating representational capacities and neuron
% allocations (as described in my report) for each category, input set,
% and group of input sets. To
%
% I've commented out a few parts that depended on the exact sets of input
% patterns I used, so it will now calculate these metrics for arbitrary
% input data. EDIT: that's not actually true. Nevermind.
%
% Unless we need them, though, we probably don't need any code 
% from here on.
% 
% -------------------------------


% the important data processing: combining the data so we can
% actually see trends, etc


% groupNames = cell(1);
% groupNames{1} = 'digits';
% groupIndices= cell(1);
% groupIndices{1} = 1;


% Control_indices = 1;
% Overlap_indices = 2:5;
% Underlap_indices = 6:9;
% Random_indices = 10:13;
% groupNames = cell(4);
% groupNames{1} = 'Control';
% groupNames{2} = 'Overlap';
% groupNames{3} = 'Underlap';
% groupNames{4} = 'Random';
% groupIndices = cell(4);
% groupIndices{1} = Control_indices; 
% groupIndices{2} = Overlap_indices;
% groupIndices{3} = Underlap_indices;
% groupIndices{4} = Random_indices;

Control_indices = 1;
Random_indices = 2:5;
groupNames = cell(2);
groupNames{1} = 'Control';
groupNames{2} = 'Random';
groupIndices = cell(2);
groupIndices{1} = Control_indices; 
groupIndices{2} = Random_indices;

firing_rate_idx = 0;
for min_firing=MIN_FIRING_RATES
    firing_rate_idx = firing_rate_idx + 1;
    FIRING_LEVELS_COUNT = length(MIN_FIRING_RATES);
    
    disp(' ')
    disp('--------------------------------------------------------------')
    fprintf('min firing rate: %.2f\n', min_firing);
    disp('--------------------------------------------------------------')
    
%     totalCols = length(MIN_FIRING_RATES) * INPUT_SETS_COUNT;
    overall_infos = zeros(1, INPUT_SETS_COUNT, length(MIN_FIRING_RATES) );
    overall_repCaps = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT, length(MIN_FIRING_RATES));
    overall_neurAllocs = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT, length(MIN_FIRING_RATES));
    overall_statDeps = zeros(1, INPUT_SETS_COUNT, length(MIN_FIRING_RATES));
    
    for thresh=THRESH_VALS
        disp('-----------------------------------------')
        fprintf('Threshold: %.2f\n', thresh);
        disp('-----------------------------------------')
		
        infos = zeros(1, INPUT_SETS_COUNT);
        repCaps = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT);
        neurAllocs = zeros(CATEGORIES_COUNT, INPUT_SETS_COUNT);
        statDeps = zeros(1, INPUT_SETS_COUNT);
        
        attrNames = cell(4);
        attrNames{1} = 'info';
        attrNames{2} = 'repCap';
        attrNames{3} = 'neurAllocs';
        attrNames{4} = 'statDeps';
        attrs = cell(4);
        attrs{1} = infos;
        attrs{2} = repCaps;
        attrs{3} = neurAllocs;
        attrs{4} = statDeps;
        
        % first, load up the data across all inputs for this firing rate
        % and threshold
        for i=1:INPUT_SETS_COUNT
            input_set_name = INPUT_SET_NAMES{i};
            dataStr = sprintf('%s_fire%d_thresh%d_infoStuff.mat', ...
                input_set_name, min_firing*1000,thresh*1000);
            d = load(dataStr);
            infos(i) = d.output_entropy;
            repCaps(:,i) = d.repCapacities;
            neurAllocs(:,i) = d.neuronAllocs;
            statDeps(i) = d.statDep;
        end
        
        overall_infos(:,:, firing_rate_idx) = infos;
        overall_repCaps(:,:, firing_rate_idx) = repCaps;
        overall_neurAllocs(:,:, firing_rate_idx) = neurAllocs;
        overall_statDeps(:,:, firing_rate_idx) = statDeps;
        
%          theStatDeps = statDeps'
%           theRepCaps = repCaps'
%           theRepCapsVect = theRepCaps(:)
%         theNeurAllocs = neurAllocs';
%         theNeurAllocsVect = theNeurAllocs(:);
%         the4thRepCaps = repCaps(4,:)'
%         the4thNeurAllocs = neurAllocs(4,:)'
%         varNeurAllocs = var(neurAllocs)'
%         varRepCaps = var(repCaps)'
        
        for i=1:length(groupIndices)
            groupName = groupNames{i};
            disp('-------------------------------')
            fprintf('Group: %s\n', groupName);
            disp('-------------------------------')
%             
            
            % uncomment whatever to see the output
            indices  = groupIndices{i};
            theInfos = infos(:, indices)';
            theRepCaps = repCaps(:, indices)'
            theNeurAllocs = neurAllocs(:, indices)'
            theStatDeps = statDeps(:, indices)';

            theRepCapsVect = theRepCaps(:);
            theNeurAllocsVect = theNeurAllocs(:);

%             if(i > 1)
%                 figure()
%     %             imagesc(theInfos)
%     %             imagesc(theRepCaps)
%                 surf(theNeurAllocs);
%     %             imagesc(theStatDeps)
% 
%                 title(groupName);
%                 xlabel('Category Number')
%                 ylabel('Input Set')
%             
%             end
        end % end group
        
        % now go through each attribute we care about and deal
        % with the data across all inputs in each group
%         for i=1:length(attrs)
%             attrName = attrNames{i}
%             dataPool = attrs{i};
%             for groupNum=1:length(groupNames)
%                 groupName = groupNames{groupNum}
%                 data = dataPool(:,groupIndices{groupNum})
%             end
%         end
    end % end thresh
end % end firing rate

PLOT_TRENDS = 0;
if (PLOT_TRENDS)
    mn_overall_infos = mean(overall_infos, 3)';
    mn_overall_repCaps = mean(overall_repCaps, 3)';
    mn_overall_neurAllocs = mean(overall_neurAllocs, 3)';
    mn_overall_statDeps = mean(overall_statDeps, 3)';

    master_repCaps = sum(mn_overall_repCaps);
    master_neurAllocs = sum(mn_overall_neurAllocs);

    skeletonPlot(1:CATEGORIES_COUNT, master_repCaps, ...
        'Representational Capacity vs Category Probability', ...
        'Category Probability', 'Representational Capacity (bits)', 'k-', 0, 3);
    setFigureFontSize(20)
    saveNetsPlot('proj_master_repCaps')

    skeletonPlot(1:CATEGORIES_COUNT, master_neurAllocs, ...
        'Neuron Allocation vs Category Probability', ...
        'Category Probability', 'Output Neurons Allocated', 'k-', 0, 3);
    setFigureFontSize(20)
    saveNetsPlot('proj_master_neurAllocs')
end

end