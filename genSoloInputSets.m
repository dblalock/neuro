function [inputObjs] = genSoloInputSets(numPatterns)
% inputObjs = genSoloInputSets(numPatterns=100)
%
% TODO document this

SetDefaultValue(1, 'numPatterns', 100);

inputObjs1 = genInputSets(numPatterns);

% this will break if the number of categories and patterns varies across
% the input sets
input1 = inputObjs1{1};
catStartIdxs = input1.catStartIdxs;
catStopIdxs = stopIndicesFromStartIndices(catStartIdxs, numPatterns);
numCategs = length(catStartIdxs);

numOnes = mean(sum(input1.inputPatterns) );
[rows, cols] = size(input1.inputPatterns);

numInputSets = length(inputObjs1);
numInputs = numCategs * numInputSets;

inputObjs = cell(numInputs, 1);

inputNum = 0;
for i=1:numCategs
    startIdx = catStartIdxs(i);
    stopIdx = catStopIdxs(i);
    inpObjs = genInputSets(numPatterns);
    
    % determine new start and stop indices
    newStopIdx = stopIdx - startIdx + 1;
    newCatStartIdxs = [1 newStopIdx];

    for j=1:length(inpObjs)
        obj = inpObjs{j};
        patterns = obj.inputPatterns;
        name = obj.name;
        
        % make all patterns but those in the current category just be noise
        newPatterns = randNoiseMat(rows, cols, numOnes);
        newPatterns(:, 1:newStopIdx) = patterns(:, startIdx:stopIdx);
        
        % modify the name so we can tell which category wasn't zeroed
        newName = [name,'_c',num2str(i)];        

        % actually use these modified values
        newInput = InputSet(newPatterns, newName, newCatStartIdxs);
        inputNum = inputNum + 1;
        inputObjs{inputNum} = newInput;
    end
end

% for i=1:numInputs
%     input = inputObjs{i}
%     fprintf('%s\n', input.name);
% end

end